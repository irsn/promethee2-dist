include: # Will provide Promethee-pack test stages
  - project: 'IRSN/Promethee2-pack'
    ref: master
    file: '.funz.gitlab-ci.yml'
  - project: 'IRSN/Promethee2-pack'
    ref: master
    file: '.snitch.gitlab-ci.yml'

stages:
  - build
  - deploy
  - test

cache:
  key: ${CI_PIPELINE_IID}
  paths:
    - .m2/repository/
    - packages
    - $CI_PROJECT_NAME-pack


merge: # Merge Promethee2-pack repo and this repository (overwrite) in $CI_PROJECT_NAME-pack
  stage: build
  image: docker:latest
  script:
    - mkdir $CI_PROJECT_NAME-pack
    - apk add curl
    # Get promethee2-pack repo content in $CI_PROJECT_NAME-pack
    - curl -H "PRIVATE-TOKEN:$PROMETHEE_TOKEN" https://gitlab.com/api/v4/projects/irsn%2Fpromethee2-pack/repository/archive --output "archive_pack.tar.gz"
    - tar -zxvf archive_pack.tar.gz && rm archive_pack.tar.gz
    - apk add rsync
    - rsync -av promethee2-pack-master*/* $CI_PROJECT_NAME-pack &&
        rm -rf promethee2-pack-master* &&
        rm -rf $CI_PROJECT_NAME-pack/plugins/io $CI_PROJECT_NAME-pack/plugins/doe $CI_PROJECT_NAME-pack/samples/* $CI_PROJECT_NAME-pack/scripts/*
    - name=`echo $CI_PROJECT_NAME | tr '[:upper:]' '[:lower:]'` && echo $name
    # Get this repository content in $CI_PROJECT_NAME-pack (overwrite)
    - curl -H "PRIVATE-TOKEN:$PROMETHEE_TOKEN" https://gitlab.com/api/v4/projects/irsn%2F$name/repository/archive --output "archive_$name.tar.gz"
    - tar -zxvf archive_$name.tar.gz && rm archive_$name.tar.gz
    - rsync -av $name-master*/* $CI_PROJECT_NAME-pack &&
        rm -rf $name-master*
    - rm -rf README.md
    - ls -la
# Not needed because kept in cache
#  artifacts:
#    paths:
#      - $CI_PROJECT_NAME-pack
#    expire_in: 1 day

get-m2-pack:
  stage: .pre
  when: always
  image: alpine:latest
  script:
    # Get artifact with .m2/repository from Promethee2
    - apk add jq curl zip
    - 'jobs_json=`curl -L -H "Private-Token: $PROMETHEE_TOKEN" "https://gitlab.com/api/v4/projects/IRSN%2FPromethee2-pack/jobs?scope=success"`'
    - 'package_job_id=`echo $jobs_json | jq -c "[.[] | select( .name == \"artifacts-m2-pack\" and .ref == \"$CI_COMMIT_REF_NAME\" )] | .[0].id"`'
    - 'curl -L -H "Private-Token: $PROMETHEE_TOKEN" "https://gitlab.com/api/v4/projects/IRSN%2FPromethee2-pack/jobs/$package_job_id/artifacts" -o artifacts-m2-pack.zip'
    - unzip artifacts-m2-pack.zip && rm artifacts-m2-pack.zip
    - unzip m2-pack-repository.zip && rm m2-pack-repository.zip

install: # Used by tests (Funz & Snitch)
  stage: deploy
  image: frekele/ant:1.10-jdk8
  variables:
    MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
    USER_HOME: "$CI_PROJECT_DIR"
  before_script:
    - echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list
    - apt-get -y update && apt-get install -y rsync
    - rsync -av $CI_PROJECT_NAME-pack/* .
  script:
    - ant install

# Should be implemented in "true" distributions (see later trigger-*)
#funz_1:
#  extends: .funz # From Promethee2-pack
#  script:
#    - funz/Funz.sh Run -if funz/PerfectGazPressure.sh -oe pressure > Run.out
#    - if [ ! "`grep 2494.2 Run.out | wc -l`" -ge 1 ]; then exit 1; fi
#  rules:
#    - if: $CI_PROJECT_NAME == "promethee2-dist"
#      when: on_success
#
#funz_2:
#  extends: .funz # From Promethee2-pack
#  script:
#    - funz/Funz.sh Run -if funz/PerfectGazPressure.sh -all -iv T=200,300,400 n=1,2 -oe pressure > Run.out
#    - if [ ! "`grep 1662.8 Run.out | wc -l`" -ge 1 ]; then exit 1; fi
#    - if [ ! "`grep 2494.2 Run.out | wc -l`" -ge 1 ]; then exit 1; fi
#    - if [ ! "`grep 6651.2 Run.out | wc -l`" -ge 1 ]; then exit 1; fi
#  rules:
#    - if: $CI_PROJECT_NAME == "promethee2-dist"
#      when: on_success
#
#snitch_1:
#  extends: .snitch # From Promethee2-pack
#  variables:
#    TESTNAME: "1"
#  rules:
#    - if: $CI_PROJECT_NAME == "promethee2-dist"
#      when: on_success
#
#snitch_2:
#  extends: .snitch # From Promethee2-pack
#  variables:
#    TESTNAME: "2"
#  rules:
#    - if: $CI_PROJECT_NAME == "promethee2-dist"
#      when: on_success


dist: # Build zip artifacts for all OS
  stage: .post
  image: frekele/ant:1.10-jdk8
  before_script:
    - echo "deb http://archive.debian.org/debian stretch main" > /etc/apt/sources.list
    - apt-get update && apt-get install -y rsync
    - rsync -av $CI_PROJECT_NAME-pack/* .
  variables:
    MAVEN_OPTS: "-Dmaven.repo.local=$CI_PROJECT_DIR/.m2/repository"
    USER_HOME: "$CI_PROJECT_DIR"
  script:
    - ant package
    - cd packages
    - apt-get install -y zip unzip
    - for o in `ls`; do echo $o; cd $o &&
        unzip Promethee*.zip -d $CI_PROJECT_NAME &&
        rm -f Promethee*.zip &&
        zip -9 -r $CI_PROJECT_NAME"_"$o".zip" $CI_PROJECT_NAME &&
        rm -rf $CI_PROJECT_NAME; cd ..; done
  artifacts:
    paths:
      - packages/*/*linux64*.zip
      - packages/*/*win64*.zip
    expire_in: 1 year


trigger-neutro:
  stage: .post
  trigger:
    project: irsn/promethee2-neutro
    strategy: depend
  rules:
    - if: $CI_PROJECT_NAME == "promethee2-dist"
      when: on_success

trigger-math:
  stage: .post
  trigger:
    project: irsn/promethee2-math
    strategy: depend
  rules:
    - if: $CI_PROJECT_NAME == "promethee2-dist"
      when: on_success

trigger-struct:
  stage: .post
  trigger:
    project: irsn/promethee2-struct
    strategy: depend
  rules:
    - if: $CI_PROJECT_NAME == "promethee2-dist"
      when: on_success

#trigger-cans:
#  stage: .post
#  trigger:
#    project: irsn/promethee2-cans
#    strategy: depend
#  rules:
#    - if: $CI_PROJECT_NAME == "promethee2-dist"
#      when: on_success

trigger-hydro:
  stage: .post
  trigger:
    project: irsn/promethee2-hydro
    strategy: depend
  rules:
    - if: $CI_PROJECT_NAME == "promethee2-dist"
      when: on_success
