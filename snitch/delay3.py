#!/usr/bin/python3

import sys, getopt

def main(argv):

    import json

    recordjson = argv
    factor = 3

    record = json.loads(open(recordjson).read())

    events = record['events']

    for e in events:
        if e['type'] == "Delay":
            e['duration'] = e['duration'] * factor

    record['events'] = events

    json.dump(record, open(recordjson, 'w'),indent=4)

if __name__ == "__main__":
    main(sys.argv[1])