## __COMMENT__

This is an example repository for a distribution of Promethee.

You should check and modify files in this repository to adapt the distribution you wish:

* 'build-config/ui-v1.plugins.list.txt': list of Funz plugins/algorithms releases to add in packaging:
    ```txt
    https://github.com/Funz/plugin-Bash/releases/download/v1.9-0/plugin-Bash.zip
    https://github.com/Funz/algorithm-Brent/releases/download/v1.9-0/algorithm-Brent.zip
    ```
* 'plugins/', 'scripts/', 'samples/': supplementary local plugins, scripts and samples to add
* 'calculator-localhost.xml': should contain local calculation configuration, like specific ports or plugins & scripts, like
    ```xml
    <CALCULATOR>

      <HOST name='localhost' port='19001'/>
      <HOST name='localhost' port='19002'/>
      <HOST name='localhost' port='19003'/>
      <HOST name='localhost' port='19004'/>
      <HOST name='localhost' port='19005'/>
      <HOST name='localhost' port='19006'/>
      <HOST name='localhost' port='19007'/>
      <HOST name='localhost' port='19008'/>
      <HOST name='localhost' port='19009'/>
      <HOST name='localhost' port='19010'/>
      <!-- some more hosts/ports if needed
      <HOST name='localhost' port='9201'/>
      <HOST name='my_coworker_host_ip' port='9201'/>
      -->
      
      <!-- if unix/osx -->
      <CODE name='Code' command='./scripts/code.sh'/>
      <CODE name='AnotherCode' command='./scripts/anothercode.sh'/>
      <!-- else if windows -->
      <CODE name='Code' command='.\scripts\code.bat'/>
      <CODE name='AnotherCode' command='.\scripts\anothercode.bat'/>

    </CALCULATOR>
    ```
* 'quotas.xml': should contain all ports given in previous 'calculator-locahost.xml'

Optionnally, you can also customize these features:

* Math engine:
  * 'Promethee2.conf': should contain just `R.server=Renjin` or `R.server=R://localhost`, if default (`R.server=R2js`) is not sufficient for your DOE plugins
* Testing:
  * '.gitlab-ci.yml': should need to uncomment some lines, for 'snitch' or 'funz' testing tasks
  * '/snitch': directory which contains Snitch recording tests, if any. Also need to uncomment '.gitlab-ci.yml' related lines
* Rendering:
  * 'plugins/file/ext-XXX.html': render files with extension '.XXX' in html (may include standalone javascript), content of file is inserted as `$$CONTENT$$` and current directory is $$DIR$$, within html.
  * 'plugins/views/MyPLot.html': render files with extension '.XXX' in html (may include standalone javascript), content of file is inserted as `$$CONTENT$$` and current directory is $$DIR$$, within html.
  * 'plugins/editor/mode-CODE-XXX.js': Ace editor pluigin to support syntaxe for files with extension '.XXX' for code 'CODE'

## __/COMMENT__

# Promethee2-*???*

This is the repository for distribution of Promethee for ??? studies. It contains Windows, Linux, OSX packages with:

* Code plugins:
  * Bash
* Syntax support:
  * Bash
* Algorithms:
  * Brent root finding
  * Gradient descent

---

Downloads:

* [Windows](https://gitlab.com/IRSN/Promethee2-dist/-/jobs/artifacts/master/raw/packages/win64b/promethee2-dist_win64b.zip?job=dist)
* [Linux](https://gitlab.com/IRSN/Promethee2-dist/-/jobs/artifacts/master/raw/packages/linux64b/promethee2-dist_linux64b.zip?job=dist)
* [OSX](https://gitlab.com/IRSN/Promethee2-dist/-/jobs/artifacts/master/raw/packages/osx64b/promethee2-dist_osx64b.zip?job=dist)

![Analytics](https://ga-beacon.appspot.com/UA-109580-11/Promethee2-dist)
